const express = require("express");
const app = express();
const morgan = require("morgan");
const cors = require("cors");
const bodyParser = require("body-parser");
const axios = require("axios");

app.use(morgan("dev"));
app.listen(process.env.PORT || 8081);
app.use(bodyParser.json());
app.use(cors());

if (process.env.NODE_ENV === "production") {
  app.use(express.static(__dirname + "/public/"));
  app.get(/.*/, (req, res) => res.sendFile(__dirname + "/public/index.html"));
}

const RESOURCE_PATH =
  "https://translated-mymemory---translation-memory.p.rapidapi.com/api/get?";

app.post("/api/translate", async (req, res) => {
  try {
    var test = await translate(req.body.text, req.body.from, req.body.to);
    res.json(test);
  } catch (error) {
    res.status(400).send({ message: "error" });
  }
});

async function translate(text, from, to) {
  return axios
    .get(RESOURCE_PATH + `q=${text}&langpair=${from}|${to}`, {
      headers: {
        "x-rapidapi-key": "cf913e2603msh9e0c991bfe3bf17p173642jsn7886cfa7bdbb",
      },
    })
    .then((result) => result.data.responseData.translatedText);
}
